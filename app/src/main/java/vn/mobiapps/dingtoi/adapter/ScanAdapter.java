package vn.mobiapps.dingtoi.adapter;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.model.model_view.ScanerModel;

/**
 * Created by Truong Thien on 3/29/2019.
 */

public class ScanAdapter extends ArrayAdapter<ScanerModel> {
    private Activity activity;
    private int resource;
    private ArrayList<ScanerModel> listInfor;

    public ScanAdapter(@NonNull Activity activity, int resource, @NonNull ArrayList<ScanerModel> listObjects) {
        super(activity, resource, listObjects);
        this.activity = activity;
        this.resource = resource;
        this.listInfor = listObjects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.activity.getLayoutInflater();
        ScanerModel model = this.listInfor.get(position);
        View row = null;
        try {
            if (model.header) {
                row = inflater.inflate(R.layout.header_detail_scan, null);
                TextView txtHeaderName = row.findViewById(R.id.txtHeaderName);
                txtHeaderName.setText(model.headername);
            } else  {
                row = inflater.inflate(this.resource, null);
                LinearLayout lyItemInfo = row.findViewById(R.id.lyItemInfo);
                ImageView imgIcon = row.findViewById(R.id.imgIcon);
                ImageView icon_status = row.findViewById(R.id.icon_status);
                TextView txtName = row.findViewById(R.id.txtName);
                TextView txtDiscription = row.findViewById(R.id.txtDiscription);
                ListView lvDetai = row.findViewById(R.id.lvDetai);
                imgIcon.setBackground(activity.getDrawable(model.icon));
                txtName.setText(model.name);
                if (position == 0) {
                    DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
                    float dp15 = 15f;
                    float dp10 = 10f;
                    float fpixels15 = metrics.density * dp15;
                    float fpixels10 = metrics.density * dp10;
                    int pixels15 = (int) (fpixels15 + 0.5f);
                    int pixels10 = (int) (fpixels10 + 0.5f);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(pixels15, pixels10, pixels15, pixels10);
                    lyItemInfo.setLayoutParams(layoutParams);
                }
                if (model.listDeail == null) {
                    lvDetai.setVisibility(View.GONE);
                    if (model.status) {
                        lyItemInfo.setBackground(activity.getDrawable(R.drawable.bg_working_1));
                        icon_status.setBackground(activity.getDrawable(R.drawable.icon_working));
                        txtDiscription.setText(model.name + " working");
                    } else {
                        lyItemInfo.setBackground(activity.getDrawable(R.drawable.bg_not_working_1));
                        icon_status.setBackground(activity.getDrawable(R.drawable.icon_not_working));
                        txtDiscription.setText(model.name + " not working");
                    }
                } else {
                    lvDetai.setVisibility(View.VISIBLE);
                    ScanDetailAdapter detailAdapter = new ScanDetailAdapter(activity, R.layout.item_detail, model.listDeail);
                    lvDetai.setAdapter(detailAdapter);
                    Boolean isCheck = false;
                    int positionError = 0;
                    for (int i = 0; i < model.listDeail.size(); i++) {
                        if (model.listDeail.get(i).value.equals("") && !model.listDeail.get(i).status) {
                            isCheck = true;
                            positionError = i;
                            break;
                        }
                    }
                    if (isCheck) {
                        if (model.listDeail.size() > 2) {
                            lyItemInfo.setBackground(activity.getDrawable(R.drawable.bg_not_working_3));
                        } else {
                            lyItemInfo.setBackground(activity.getDrawable(R.drawable.bg_not_working_2));
                        }
                        icon_status.setBackground(activity.getDrawable(R.drawable.icon_not_working));
                        txtDiscription.setVisibility(View.VISIBLE);
                        txtDiscription.setText(model.listDeail.get(positionError).name + " not working");
                    } else {
                        if (model.listDeail.size() > 2) {
                            lyItemInfo.setBackground(activity.getDrawable(R.drawable.bg_working_3));
                        } else {
                            lyItemInfo.setBackground(activity.getDrawable(R.drawable.bg_working_2));
                        }
                        icon_status.setBackground(activity.getDrawable(R.drawable.icon_working));
                        txtDiscription.setVisibility(View.GONE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }
}
