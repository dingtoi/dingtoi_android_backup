package vn.mobiapps.dingtoi.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.model.model_view.ScanerDetailModel;
public class ScanDetailAdapter extends ArrayAdapter<ScanerDetailModel> {

    private Activity activity;
    private int resource;
    private ArrayList<ScanerDetailModel> listDetail;

    public ScanDetailAdapter(@NonNull Activity activity, int resource, @NonNull ArrayList<ScanerDetailModel> objects) {
        super(activity, resource, objects);
        this.activity = activity;
        this.resource = resource;
        this.listDetail = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.activity.getLayoutInflater();
        View row = inflater.inflate(this.resource, null);
        ScanerDetailModel model = this.listDetail.get(position);
        TextView txtName = row.findViewById(R.id.txtName);
        TextView txtValue = row.findViewById(R.id.txtValue);
        ImageView icon_status = row.findViewById(R.id.icon_status);

        txtName.setText(model.name);
        if (model.value != null && !model.value.equals("")) {
            icon_status.setVisibility(View.GONE);
            txtValue.setVisibility(View.VISIBLE);
            txtValue.setText(model.value);
        } else {
            icon_status.setVisibility(View.VISIBLE);
            txtValue.setVisibility(View.GONE);
            if (model.status) {
                icon_status.setBackground(activity.getDrawable(R.drawable.icon_working));
            } else {
                icon_status.setBackground(activity.getDrawable(R.drawable.icon_not_working));
            }
        }
        return row;
    }
}
