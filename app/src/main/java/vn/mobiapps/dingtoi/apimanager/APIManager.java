package vn.mobiapps.dingtoi.apimanager;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import vn.mobiapps.dingtoi.model.model_data.PhoneModel;
import vn.mobiapps.dingtoi.model.model_view.LoginRequest;
import vn.mobiapps.dingtoi.model.model_view.ResponseErrorModel;

public class APIManager {

    public static final String errorDescription = "Server đang bận, vui lòng thử lại sau";
    public static final String DescriptionSessiontimout = "Phiên làm việc đã hết vui lòng đăng nhập lại";
    private static String TAG = "Request ";

    public static String mAccessToken = "";
    public static String deviceID = null;
    public static String mAccessToken_FB = null;
    public static String mlanguage = "vi";

    //Url Server Dev
    static final String urlServer = "http://dingtoi.com:30080";

    private static String getUrl() {
        return urlServer;
    }

    //================Url====================
    //Authentication
    static final String url_Authentication_Login_Post = getUrl() + "/API/login";
    //checkTransactionCode
    static final String url_checkTransactionCode_Get = getUrl() + "/api/v1/transaction/check";
    //check Text Inbound
    static final String url_checkTextInbound_Post = getUrl() + "/api/v1/transaction/text/check/inbound";
    //check Voice Inbound
    static final String url_checkVoiceInbound_Post = getUrl() + "/api/v1/transaction/voice/check/inbound";


    //================API====================

    private static void sendRequestPost(ModelConfig config, final APIInterface.onDelegate delegate) {
        try {
            APIManagerBackend.HTTPRequestPOST(config, new APIInterface.onDelegate() {
                @Override
                public void onSuccess(Object result) {
                    delegate.onSuccess(result);
                }

                @Override
                public void onError(ResponseErrorModel error) {
                    delegate.onError(error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void sendRequestGet(ModelConfig config, final APIInterface.onDelegate delegate) {
        try {
            APIManagerBackend.HTTPRequestGET(config, new APIInterface.onDelegate() {
                @Override
                public void onSuccess(Object result) {
                    delegate.onSuccess(result);
                }

                @Override
                public void onError(ResponseErrorModel error) {
                    delegate.onError(error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void sendRequestPut(ModelConfig config, final APIInterface.onDelegate delegate) {
        try {
            APIManagerBackend.HTTPRequestPUT(config, new APIInterface.onDelegate() {
                @Override
                public void onSuccess(Object result) {
                    delegate.onSuccess(result);
                }

                @Override
                public void onError(ResponseErrorModel error) {
                    delegate.onError(error);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // APi login
    public static void Authentication_Login(LoginRequest request, final APIInterface.onDelegate delegate) {
        try {
            JSONObject obj = new JSONObject(new Gson().toJson(request, LoginRequest.class));
            ModelConfig config = new ModelConfig();
            config.linkAPI = url_Authentication_Login_Post + "?phone_number=" + request.phone_number + "&password=" + request.password;
            config.apiName = "Authentication_Login";
            config.isAuthorization = false;
            config.jsonObject = obj;
            sendRequestGet(config, delegate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void checkTransactionCode(String transactionCode, final APIInterface.onDelegate delegate) {
        try {
            ModelConfig config = new ModelConfig();
            config.linkAPI = url_checkTransactionCode_Get + "/" + transactionCode;
            config.apiName = "checkTransactionCode";
            config.isAuthorization = false;
            sendRequestGet(config, delegate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkTextInbound(PhoneModel phoneModel, final APIInterface.onDelegate delegate) {
        try {
            JSONObject obj = new JSONObject(new Gson().toJson(phoneModel, PhoneModel.class));
            ModelConfig config = new ModelConfig();
            config.linkAPI = url_checkTextInbound_Post;
            config.apiName = "checkTextInbound";
            config.isAuthorization = false;
            config.jsonObject = obj;
            sendRequestPost(config, delegate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkVoiceInbound(PhoneModel phoneModel, final APIInterface.onDelegate delegate) {
        try {
            JSONObject obj = new JSONObject(new Gson().toJson(phoneModel, PhoneModel.class));
            ModelConfig config = new ModelConfig();
            config.linkAPI = url_checkVoiceInbound_Post;
            config.apiName = "checkVoiceInbound";
            config.isAuthorization = false;
            config.jsonObject = obj;
            sendRequestPost(config, delegate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
