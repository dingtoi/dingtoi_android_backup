package vn.mobiapps.dingtoi.apimanager;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import vn.mobiapps.dingtoi.model.model_view.ResponseErrorModel;
import vn.mobiapps.dingtoi.utils.Constant;
import vn.mobiapps.dingtoi.utils.ShareReference;

import static vn.mobiapps.dingtoi.apimanager.APIManager.errorDescription;
import static vn.mobiapps.dingtoi.apimanager.APIManager.mAccessToken;

public class APIManagerBackend {

    private static HashMap<String, String> headers(Boolean isAuthorization) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json; charset=utf-8");
        headers.put("Accept", "application/json; charset=utf-8");
        if (isAuthorization && mAccessToken != null && !mAccessToken.equals("")) {
            headers.put("Authorization", "Bearer " + mAccessToken);
        }
        return headers;
    }

    public static void HTTPRequestGET(final ModelConfig config, final APIInterface.onDelegate delegate) {
        if (ShareReference.isCheckLog) {
            Log.e("Request: " + config.apiName, config.linkAPI);
        }
        RequestQueue queue = Volley.newRequestQueue(Constant.context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, config.linkAPI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (ShareReference.isCheckLog) {
                                Log.e("Response: " + config.apiName, response);
                            }
                            delegate.onSuccess(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                            ResponseErrorModel result = new ResponseErrorModel("", "", errorDescription);
                            delegate.onError(result);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (ShareReference.isCheckLog) {
                            Log.e("Response: " + config.apiName, error.toString());
                        }
                        // As of f605da3 the following should work
                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                if (ShareReference.isCheckLog) {
                                    Log.e(config.apiName + " :", error.toString());
                                }
                                ResponseErrorModel result = new Gson().fromJson(res.toString(), ResponseErrorModel.class);
                                if (result == null) {
                                    result = new ResponseErrorModel("", "", errorDescription);
                                }
                                delegate.onError(result);
                            } catch (Exception e1) {
                                // Couldn't properly decode data to string
                                e1.printStackTrace();
                                ResponseErrorModel result = new ResponseErrorModel("", "", errorDescription);
                                delegate.onError(result);
                            }
                        } else {
                            if (response != null && response.statusCode == Integer.parseInt(Constant.RESPONE_CODE)) {
                                try {
                                    String body = new String(response.data, "UTF-8");
                                    ResponseErrorModel result = new Gson().fromJson(body, ResponseErrorModel.class);
                                    delegate.onError(result);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                ResponseErrorModel result = new ResponseErrorModel("", "", errorDescription);
                                delegate.onError(result);
                            }
                        }
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                if (ShareReference.isCheckLog) {
                    Log.e(">>>  Status Code: ", mStatusCode + "");
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers(config.isAuthorization);
            }
        };
        stringRequest.setRetryPolicy(new
                DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public static void HTTPRequestPOST(final ModelConfig config, final APIInterface.onDelegate delegate) {
        if (ShareReference.isCheckLog) {
            Log.e("Reques:" + config.apiName, config.jsonObject.toString());
        }
        RequestQueue queue = Volley.newRequestQueue(Constant.context);
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, config.linkAPI, config.jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (ShareReference.isCheckLog) {
                                Log.e("Response: " + config.apiName, response.toString());
                            }
                            delegate.onSuccess(response);
                        } catch (Exception e) {
                            ResponseErrorModel result = new ResponseErrorModel("", "", errorDescription);
                            delegate.onError(result);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (ShareReference.isCheckLog) {
                            Log.e("Response: " + config.apiName, error.toString());
                        }
                        // As of f605da3 the following should work
                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                if (ShareReference.isCheckLog) {
                                    Log.e(">>>  Error Logout: ", res.toString());
                                }
                                ResponseErrorModel result = new Gson().fromJson(res.toString(), ResponseErrorModel.class);
                                if (result == null) {
                                    result = new ResponseErrorModel("", "", errorDescription);
                                }
                                delegate.onError(result);
                            } catch (Exception e1) {
                                // Couldn't properly decode data to string
                                e1.printStackTrace();
                                ResponseErrorModel result = new ResponseErrorModel("", "", errorDescription);
                                delegate.onError(result);
                            }
                        } else {
                            if (response != null && response.statusCode == Integer.parseInt(Constant.RESPONE_CODE)) {
                                try {
                                    String body = new String(response.data, "UTF-8");
                                    ResponseErrorModel result = new Gson().fromJson(body, ResponseErrorModel.class);
                                    delegate.onError(result);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                ResponseErrorModel result = new ResponseErrorModel("", "", errorDescription);
                                delegate.onError(result);
                            }
                        }
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                if (ShareReference.isCheckLog) {
                    Log.e(">>>  Status Code: ", mStatusCode + "");
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers(config.isAuthorization);
            }
        };
        stringRequest.setRetryPolicy(new
                DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public static void HTTPRequestPUT(final ModelConfig config, final APIInterface.onDelegate delegate) {
        if (ShareReference.isCheckLog) {
            Log.e("Reques:" + config.apiName, config.jsonObject.toString());
        }
        RequestQueue queue = Volley.newRequestQueue(Constant.context);
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.PUT, config.linkAPI, config.jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (ShareReference.isCheckLog) {
                                Log.e("Response: " + config.apiName, response.toString());
                            }
                            delegate.onSuccess(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                            ResponseErrorModel result = new ResponseErrorModel("", "", errorDescription);
                            delegate.onError(result);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (ShareReference.isCheckLog) {
                            Log.e("Response: " + config.apiName, error.toString());
                        }
                        // As of f605da3 the following should work
                        NetworkResponse response = error.networkResponse;
                        if (error instanceof ServerError && response != null) {
                            try {
                                String res = new String(response.data,
                                        HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                                if (ShareReference.isCheckLog) {
                                    Log.e("Response: " + config.apiName, error.toString());
                                }
                                ResponseErrorModel result = new Gson().fromJson(res.toString(), ResponseErrorModel.class);
                                if (result == null) {
                                    result = new ResponseErrorModel("", "", errorDescription);
                                }
                                delegate.onError(result);
                            } catch (Exception e1) {
                                // Couldn't properly decode data to string
                                e1.printStackTrace();
                                ResponseErrorModel result = new ResponseErrorModel("", "", errorDescription);
                                delegate.onError(result);
                            }
                        } else {
                            if (response != null && response.statusCode == Integer.parseInt(Constant.RESPONE_CODE)) {
                                try {
                                    String body = new String(response.data, "UTF-8");
                                    ResponseErrorModel result = new Gson().fromJson(body, ResponseErrorModel.class);
                                    delegate.onError(result);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                ResponseErrorModel result = new ResponseErrorModel("", "", errorDescription);
                                delegate.onError(result);
                            }
                        }
                    }
                }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                if (ShareReference.isCheckLog) {
                    Log.e(">>>  Status Code: ", mStatusCode + "");
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers(true);
            }
        };
        stringRequest.setRetryPolicy(new
                DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }
}
