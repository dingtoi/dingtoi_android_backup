package vn.mobiapps.dingtoi.apimanager;

import android.graphics.Bitmap;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

public class ModelConfig implements Serializable {
    public String linkAPI;
    public String apiName;
    public Boolean isAuthorization;
    public HashMap<String,String> queryParameter;
    public JSONObject jsonObject;
    public Bitmap avatar;
}
