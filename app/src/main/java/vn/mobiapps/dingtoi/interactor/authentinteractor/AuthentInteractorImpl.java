package vn.mobiapps.dingtoi.interactor.authentinteractor;

import vn.mobiapps.dingtoi.apimanager.APIInterface;
import vn.mobiapps.dingtoi.apimanager.APIManager;
import vn.mobiapps.dingtoi.interfaces.BaseInterfaceImpl;
import vn.mobiapps.dingtoi.interfaces.IBaseListener;
import vn.mobiapps.dingtoi.model.model_view.LoginRequest;
import vn.mobiapps.dingtoi.model.model_view.ResponseErrorModel;

/**
 * Created by Truong Thien on 4/19/2019.
 */

public class AuthentInteractorImpl extends BaseInterfaceImpl implements IAuthentInteractor{

    private IBaseListener interactorListener;

    public AuthentInteractorImpl(IBaseListener interactorListener ) {
        if (interactorListener != null) {
            this.interactorListener = interactorListener;
        }
    }

    @Override
    public void login(LoginRequest loginRequest) {
        APIManager.Authentication_Login(loginRequest, new APIInterface.onDelegate() {
            @Override
            public void onSuccess(Object result) {
                interactorListener.onSuccess(result);
            }

            @Override
            public void onError(ResponseErrorModel error) {
                interactorListener.onError(error);
            }
        });
    }
}
