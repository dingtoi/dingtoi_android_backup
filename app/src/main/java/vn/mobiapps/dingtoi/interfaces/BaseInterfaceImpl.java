package vn.mobiapps.dingtoi.interfaces;

import vn.mobiapps.dingtoi.model.model_data.PhoneModel;
import vn.mobiapps.dingtoi.model.model_view.LoginRequest;

/**
 * Created by Truong Thien on 4/19/2019.
 */

public class BaseInterfaceImpl implements IBaseInterface {

    @Override
    public void login(LoginRequest loginRequest) {

    }

    @Override
    public void checkTransactionCode(String transactionCode) {

    }

    @Override
    public void checkTextInbound(PhoneModel phoneModel) {

    }

    @Override
    public void checkVoiceInbound(PhoneModel phoneModel) {

    }
}
