package vn.mobiapps.dingtoi.interfaces;

import vn.mobiapps.dingtoi.model.model_data.PhoneModel;
import vn.mobiapps.dingtoi.model.model_view.LoginRequest;

/**
 * Created by Truong Thien on 4/4/2019.
 */

public interface IBaseInterface {
    // Auth
    void login(LoginRequest loginRequest);
    /*Scan Device*/
    void checkTransactionCode(String transactionCode);
    /*Check Text Inbound*/
    void checkTextInbound(PhoneModel phoneModel);
    /*Check Voice Inbound*/
    void checkVoiceInbound(PhoneModel phoneModel);
}
