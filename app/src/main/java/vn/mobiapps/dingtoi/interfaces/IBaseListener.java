package vn.mobiapps.dingtoi.interfaces;

/**
 * Created by Truong Thien on 4/4/2019.
 */

public interface IBaseListener<T>{
    void onSuccess(T onSuccess);
    void onError(T onError);
}
