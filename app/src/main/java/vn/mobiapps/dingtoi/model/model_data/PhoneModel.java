package vn.mobiapps.dingtoi.model.model_data;

import java.io.Serializable;

public class PhoneModel implements Serializable {
    public String phone;

    public PhoneModel(String phone) {
        this.phone = phone;
    }
}
