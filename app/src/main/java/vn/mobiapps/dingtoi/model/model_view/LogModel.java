package vn.mobiapps.dingtoi.model.model_view;

import java.io.Serializable;

public class LogModel implements Serializable {
    public String id;
    public String name;
    public String value;

    public LogModel(String id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }
}
