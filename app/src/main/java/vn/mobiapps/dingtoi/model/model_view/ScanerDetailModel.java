package vn.mobiapps.dingtoi.model.model_view;

import java.io.Serializable;

public class ScanerDetailModel implements Serializable {
    public String name;
    public String value;
    public boolean status;

    public ScanerDetailModel(String name, String value, boolean status) {
        this.name = name;
        this.value = value;
        this.status = status;
    }
}
