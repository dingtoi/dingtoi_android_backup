package vn.mobiapps.dingtoi.model.model_view;

import java.io.Serializable;
import java.util.ArrayList;

public class ScanerModel implements Serializable {
    public int icon;
    public String name;
    public String discription;
    public ArrayList<ScanerDetailModel> listDeail;
    public Boolean status;
    public Boolean header = false;
    public String headername;

    public ScanerModel(int icon, String name, String discription, ArrayList<ScanerDetailModel> listDeail, Boolean status) {
        this.icon = icon;
        this.name = name;
        this.discription = discription;
        this.listDeail = listDeail;
        this.status = status;
    }

    public ScanerModel(Boolean header, String headername) {
        this.header = header;
        this.headername = headername;
    }
}
