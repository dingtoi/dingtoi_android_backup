package vn.mobiapps.dingtoi.presenter.authentpresenter;

import vn.mobiapps.dingtoi.interactor.authentinteractor.AuthentInteractorImpl;
import vn.mobiapps.dingtoi.interactor.authentinteractor.IAuthentInteractor;
import vn.mobiapps.dingtoi.interfaces.BaseInterfaceImpl;
import vn.mobiapps.dingtoi.interfaces.IBaseListener;
import vn.mobiapps.dingtoi.model.model_view.LoginRequest;
import vn.mobiapps.dingtoi.view.base.IBaseView;

/**
 * Created by Truong Thien on 4/19/2019.
 */

public class AuthentPresenterImpl extends BaseInterfaceImpl implements IAuthentPresenter, IBaseListener {

    IBaseView viewListener;
    IAuthentInteractor interactor;

    public AuthentPresenterImpl(IBaseView viewListener) {
        try {
            if (viewListener != null) {
                this.viewListener = viewListener;
                interactor = new AuthentInteractorImpl(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void login(LoginRequest loginRequest) {
        viewListener.nameRequest("login");
        viewListener.showLoading();
        interactor.login(loginRequest);
    }

    @Override
    public void onSuccess(Object onSuccess) {
        viewListener.hideLoading();
        viewListener.onSuccess(onSuccess);
    }

    @Override
    public void onError(Object onError) {
        viewListener.hideLoading();
        viewListener.onError(onError);
    }
}
