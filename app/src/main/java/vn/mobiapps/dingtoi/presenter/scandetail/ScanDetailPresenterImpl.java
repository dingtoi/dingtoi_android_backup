package vn.mobiapps.dingtoi.presenter.scandetail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.fingerprint.FingerprintManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import vn.mobiapps.dingtoi.interactor.scandetail.IScanDetailInteractor;
import vn.mobiapps.dingtoi.interactor.scandetail.ScanDetailInteractorImpl;
import vn.mobiapps.dingtoi.interfaces.BaseInterfaceImpl;
import vn.mobiapps.dingtoi.interfaces.IBaseListener;
import vn.mobiapps.dingtoi.model.model_data.PhoneModel;
import vn.mobiapps.dingtoi.model.model_view.LogModel;
import vn.mobiapps.dingtoi.view.base.IBaseView;

import static android.content.Context.MODE_APPEND;

public class ScanDetailPresenterImpl extends BaseInterfaceImpl implements IScanDetailPresenter, IBaseListener {
    private IBaseView viewListener;
    private IScanDetailInteractor interactor;
    private Activity activity;
    private Camera mCamera = null;
    private Preview mPreview;

    public ScanDetailPresenterImpl(IBaseView viewListener, Activity activity) {
        try {
            if (viewListener != null) {
                this.viewListener = viewListener;
                this.activity = activity;
                interactor = new ScanDetailInteractorImpl(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Request API*/
    @Override
    public void checkTransactionCode(String transactionCode) {
        viewListener.showLoading();
        interactor.checkTransactionCode(transactionCode);
    }

    @Override
    public void checkTextInbound(PhoneModel phoneModel) {
        viewListener.nameRequest("checkTextInbound");
        viewListener.showLoading();
        interactor.checkTextInbound(phoneModel);
    }

    @Override
    public void checkVoiceInbound(PhoneModel phoneModel) {
        viewListener.nameRequest("checkVoiceInbound");
        viewListener.showLoading();
        interactor.checkVoiceInbound(phoneModel);
    }

    @Override
    public void onSuccess(Object onSuccess) {
        viewListener.hideLoading();
        viewListener.onSuccess(onSuccess);
    }

    @Override
    public void onError(Object onError) {
        viewListener.hideLoading();
        viewListener.onError(onError);
    }

    /*Check camera Back*/
    public boolean hasBackCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK /*&& safeCameraOpen(cameraInfo.facing)*/) {
                return true;
            }
        }
        return false;
    }

    public boolean safeCameraOpen(int id) {
        boolean qOpened = false;
        try {
            releaseCameraAndPreview();
            mCamera = Camera.open(id);
            qOpened = (mCamera != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qOpened;
    }

    private void releaseCameraAndPreview() {
        mPreview = new Preview(activity);
        mPreview.setCamera(null);
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    /*Check Bluetooth has Support*/
    public boolean isBluetoothSupported() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean isHasFingterprinter() {
        FingerprintManager fingerprintManager = (FingerprintManager) activity.getSystemService(Context.FINGERPRINT_SERVICE);
        if (!fingerprintManager.isHardwareDetected()) {
            // Device doesn't support fingerprint authentication
            return false;
        }
        return true;
    }

    public boolean isWifiSupported() {
        ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI) == null) {
            return false;
        }
        return true;
    }

    /*get Total Internal Stored*/
    public int getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long totalBlocks = stat.getBlockCountLong();
        Float tmp = (totalBlocks * blockSize) / Float.parseFloat((1024 * 1024 * 1024) + "");
        Double rs = Math.round(tmp * 100.0) / 100.0;
        int[] listSize = {8, 16, 32, 64, 128, 256, 512, 1024};
        for (int i = 0; i < listSize.length; i++) {
            if (rs < listSize[i]) {
                return listSize[i];
            }
        }
        return Integer.parseInt(rs.toString());
    }

    /*get Free Storage*/
    public Double getFreeDiskStorage() {
        try {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                File path = Environment.getDataDirectory();
                StatFs stat = new StatFs(path.getPath());
                long blockSize = stat.getBlockSizeLong();
                long availableBlocks = stat.getAvailableBlocksLong();
                Float tmp = (availableBlocks * blockSize) / Float.parseFloat((1024 * 1024 * 1024) + "");
                Double rs = Math.round(tmp * 100.0) / 100.0;
                return rs;
            } else {
                return -1.0;
            }
        } catch (Exception e) {
            return -1.0;
        }
    }

    public String latestVersion() {
        return Build.VERSION.RELEASE;
    }

    public String hardware() {
        return Build.HARDWARE.toUpperCase();
    }

    /*Check Flash has Support*/
    public boolean isFlashSupported() {
        return activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public boolean hasTelephony() {
        return activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    /*Can use function hasTelephony or hasGsmOrCdma for check SMS functional*/
    public boolean hasGsmOrCdma() {
        PackageManager pm = activity.getPackageManager();
        boolean gsmSupported = pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_GSM);
        boolean cdmaSupported = pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY_CDMA);
        return gsmSupported || cdmaSupported;
    }

    public boolean hasControlVolume() {
        try {
            AudioManager audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
            int currentVolumePercentage = 0;
            if (audioManager != null) {
                int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
                int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
                currentVolumePercentage = 100 * currentVolume / maxVolume;
            }
            return currentVolumePercentage >= 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean hasAudioOutput() {
        try {
            AudioManager audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
            PackageManager packageManager = activity.getPackageManager();
            if (audioManager != null) {
                if (audioManager.isBluetoothA2dpOn()) {
                    // Adjust output for Bluetooth.
                    return true;
                } else if (audioManager.isBluetoothScoOn()) {
                    // Adjust output for Bluetooth of sco.
                    return true;
                } else if (audioManager.isWiredHeadsetOn()) {
                    // Adjust output for headsets
                    return true;
                } else if (audioManager.isSpeakerphoneOn()) {
                    // Adjust output for Speakerphone.
                    return true;
                } else if (packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_OUTPUT)) {
                    // Has internal speaker or other form of audio output.
                    return true;
                } else {
                    // No device for audio output.
                    return false;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getDeviceName() {
        if (isBluetoothSupported()) {
            BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
            return myDevice.getName();
        } else {
            return Build.MODEL;
        }
    }

    private int getPointScanDetail() {
        int points = 50;
        //Camera
        if (!hasBackCamera()) {
            points = points - 10;
        }
        //Flash working
        if (!isFlashSupported()) {
            points = points - 5;
        }
        //Volume adjustment working
        if (!hasControlVolume()) {
            points = points - 10;
        }
        //Wi-Fi working
        if (!isWifiSupported()) {
            points = points - 5;
        }
        //Bluetooth working
        if (!isBluetoothSupported()) {
            points = points - 5;
        }
        //Finger scanner working
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!isHasFingterprinter()) {
                points = points - 2;
            }
        }
        //Touchscreen working
        //points = points - 50;
        // Hardware verification (processor, OS, storage GB)
        double storageUsed = getTotalInternalMemorySize() - getFreeDiskStorage();
        double roundStorageUsed = Math.round(storageUsed * 100.0) / 100.0;
        if (roundStorageUsed < 0 || latestVersion() == null || hardware() == null) {
            points = points - 15;
        }
        //Battery full charge capacity
        return points;
    }

    public int diamondRating(int pointPhysicalGrading) {
        int rating = 0;
        int points = getPointScanDetail() + pointPhysicalGrading;
        Log.v("Points PhysicalGrading", pointPhysicalGrading + "");
        Log.v("Points Scan", getPointScanDetail() + "");
        if (points < 60) {
            rating = 1;
        } else if (points <= 69) {
            rating = 2;
        } else if (points <= 79) {
            rating = 3;
        } else if (points <= 89) {
            rating = 4;
        } else if (points <= 100) {
            rating = 5;
        }
        return rating;
    }

    public void writeLogFile(ArrayList<LogModel> listDataLog) {
        File logFile = new File(Environment.getExternalStorageDirectory(), "dingtoi");
        if (!logFile.exists()) {
            logFile.mkdirs();
        }
        File gpxfile = new File(logFile, "DingToi.txt");
        try {
            FileWriter writer = new FileWriter(gpxfile);
            if (listDataLog != null && listDataLog.size() > 0) {
                for (int i = 0; i < listDataLog.size(); i++) {
                    writer.append(listDataLog.get(i).id + " " + listDataLog.get(i).name + " " + listDataLog.get(i).value + "\n");
                }
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    public String getTelephoneNumber() {
        try {
            TelephonyManager tMgr = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
            return tMgr.getLine1Number();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
