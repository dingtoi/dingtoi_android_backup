package vn.mobiapps.dingtoi.view.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.utils.Constant;

public class FlashActivity extends AppCompatActivity {
    private final int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);
        Constant.context = this;
        this.GoToLoginActivity();
    }

    private void GoToLoginActivity() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(FlashActivity.this, StartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
