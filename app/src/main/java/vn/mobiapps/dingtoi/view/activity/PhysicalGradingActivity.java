package vn.mobiapps.dingtoi.view.activity;

import android.os.Bundle;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.utils.Constant;
import vn.mobiapps.dingtoi.view.base.BaseActivity;
import vn.mobiapps.dingtoi.view.fragment.physicalgrading.PhysicalGradingFragment;

public class PhysicalGradingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_physical_grading);
        Constant.context = this;
        pushFragment(new PhysicalGradingFragment());
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        this.finish();
    }
}
