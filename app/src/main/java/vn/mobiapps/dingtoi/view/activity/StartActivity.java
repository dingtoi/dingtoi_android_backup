package vn.mobiapps.dingtoi.view.activity;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.utils.Constant;
import vn.mobiapps.dingtoi.view.base.BaseActivity;
import vn.mobiapps.dingtoi.view.fragment.start.InputTransactionFragment;
import vn.mobiapps.dingtoi.view.fragment.start.StartFragment;

public class StartActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Constant.context = this;
        boolean isScanAgain = getIntent().getBooleanExtra("isScanAgain", false);
        if (isScanAgain) {
            pushFragment(new InputTransactionFragment());
        } else {
            pushFragment(new StartFragment());
        }
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        this.finish();
    }
}
