package vn.mobiapps.dingtoi.view.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.utils.Constant;
import vn.mobiapps.dingtoi.view.base.BaseActivity;
import vn.mobiapps.dingtoi.view.fragment.touchscreen.TouchScreenFragment;

public class TouchScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch_screen);
        Constant.context = this;
        pushFragment(new TouchScreenFragment());
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        this.finish();
    }

}
