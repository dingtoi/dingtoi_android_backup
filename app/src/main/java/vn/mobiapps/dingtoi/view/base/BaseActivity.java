package vn.mobiapps.dingtoi.view.base;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.model.model_view.ResponseErrorModel;
import vn.mobiapps.dingtoi.utils.Constant;
import vn.mobiapps.dingtoi.utils.ShareReference;
import vn.mobiapps.dingtoi.utils.Utils;
import vn.mobiapps.dingtoi.widgets.PushPopFragment;
import vn.mobiapps.dingtoi.widgets.dialog.DialogFactory;
import vn.mobiapps.dingtoi.widgets.dialog.TranslucentDialog;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener, IBaseActivity {

    final int SPLASH_TIME_OUT = 500;
    public FrameLayout layout_base, layout_main, layout_menu, layout_popup, layout_progress;
    public PushPopFragment pushPop;
    private List<BaseFragment> listFragment = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (pushPop == null) {
            pushPop = new PushPopFragment();
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        FrameLayout baseLayout = (FrameLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        layout_main = baseLayout.findViewById(R.id.layout_main);
        layout_popup = baseLayout.findViewById(R.id.layout_popup);
        layout_progress = baseLayout.findViewById(R.id.layout_progress);
        pushPop.setIDContentReplace(R.id.layout_main);//Notes
        listFragment = new ArrayList<BaseFragment>();
        getLayoutInflater().inflate(layoutResID, layout_main, true);
        super.setContentView(baseLayout);
        setUpview();
        onClickListioner();
    }

    private void setUpview() {
    }

    private void onClickListioner() {
    }

    public FrameLayout setContentViewPopup(@LayoutRes int layoutResID) {
        layout_popup.removeAllViews();
        getLayoutInflater().inflate(layoutResID, layout_popup, true);
        return layout_popup;
    }

    public void showPopup(){
        if (layout_popup.getVisibility() == View.GONE) {
            layout_popup.setVisibility(View.VISIBLE);
            Animation slideright = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
            layout_popup.setAnimation(slideright);
        }
    }

    public void hidePopup(){
        if (layout_popup.getVisibility() == View.VISIBLE) {
            layout_popup.setVisibility(View.GONE);
            Animation slideleft = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
            layout_popup.setAnimation(slideleft);
        }
    }

    public FrameLayout setContentViewMenu(@LayoutRes int layoutResID) {
        layout_menu.removeAllViews();
        getLayoutInflater().inflate(layoutResID, layout_menu, true);
        return layout_menu;
    }

    public void showMenu() {
        if (layout_menu.getVisibility() == View.GONE) {
            layout_menu.setVisibility(View.VISIBLE);
            Animation slideright = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
            layout_menu.setAnimation(slideright);
        }
    }

    public void hideMenu() {
        if (layout_menu.getVisibility() == View.VISIBLE) {
            layout_menu.setVisibility(View.GONE);
            Animation slideleft = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
            layout_menu.setAnimation(slideleft);
        }
    }

    public void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) this.getBaseContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            View focusedView = this.getCurrentFocus();
            if (focusedView != null) {
                imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layout_progress.setVisibility(View.VISIBLE);
            }
        });
    }

    public void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layout_progress.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View v) {
    }

    public void CheckNetworkRequest(final BaseActivity baseActivity, final BaseFragment.CheckNetworkListener listener) {
        try {
            if (Utils.isNetworkAvailable(this)) {
                listener.Connected();
            } else {
                CheckNetworkRequestAgain(getString(R.string.dialog_not_internet), baseActivity, listener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CheckNetworkRequestAgain(String errorMessage, final BaseActivity baseActivity, final BaseFragment.CheckNetworkListener listener) {
        try {
            DialogFactory.dialog_TryNetAgain(baseActivity, errorMessage, new DialogFactory.DialogListener.RetryNetListener() {
                @Override
                public void retry(final TranslucentDialog dialog) {
                    if (Utils.isNetworkAvailable(BaseActivity.this)) {
                        listener.Connected();
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        baseActivity.showProgressBar();
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                baseActivity.hideProgressBar();
                                CheckNetworkRequestAgain(getString(R.string.dialog_not_internet), baseActivity, listener);
                            }
                        }, SPLASH_TIME_OUT);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading() {
        showProgressBar();
    }

    @Override
    public void hideLoading() {
        hideProgressBar();
    }

    @Override
    public void nameRequest(String style) {

    }

    @Override
    public void onSuccess(Object objectSuccess) {

    }

    @Override
    public void onError(Object objectError) {
        try {
            final String message = ((ResponseErrorModel) objectError).getMessenge();
            String code = ((ResponseErrorModel) objectError).getCode();
            if (ShareReference.isLogin) {
                if (code.equals(Constant.RESPONE_CODE)) {
                    DialogFactory.createMessageDialogSessionTimeout(this, message, code);
                } else {
                    showMessage(message);
                }
            } else {
                showMessage(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void showMessage(final String message) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogFactory.dialog_Message(BaseActivity.this, message);
            }
        });
    }

    @Override
    public void updateUI(Object object) {

    }

    /*Method active finish Activity*/
    public void endFragment() {
    }

    //=====================================Push Pop Fragment==============================
    public void pushFragment(BaseFragment fragment) {
        pushPop.pushFragment(fragment, listFragment, this);
    }

    public void pushFragmentAnimation(BaseFragment fragment) {
        pushPop.pushFragmentAnimotion(fragment, listFragment, this);
    }

    public void popFragment() {
        pushPop.popFragment(listFragment, this);
    }

    public void popToIndex(int foundIndex) {
        pushPop.popToIndex(foundIndex, listFragment, this);
    }

    public int isExistFragment(String className) {
        return pushPop.isExistFragment(className, listFragment);
    }

    //=====================================End Push Pop Fragment==============================
}
