package vn.mobiapps.dingtoi.view.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import vn.mobiapps.dingtoi.model.model_view.ResponseErrorModel;
import vn.mobiapps.dingtoi.utils.Constant;
import vn.mobiapps.dingtoi.utils.ShareReference;
import vn.mobiapps.dingtoi.widgets.dialog.DialogFactory;

public abstract class BaseFragment extends Fragment implements IBaseView, View.OnClickListener {
    protected abstract int resLayout();
    protected abstract void createFragment();
    protected abstract void setupFragment(View view);
    protected abstract void setOnClickListenerFragment();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(resLayout(), container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupFragment(view);
        setOnClickListenerFragment();
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void showLoading() {
        showProgressBar();
    }

    @Override
    public void hideLoading() {
        hideProgressBar();
    }

    @Override
    public void nameRequest(String style) {

    }

    @Override
    public void onSuccess(Object objectSuccess) {

    }

    @Override
    public void onError(Object objectError) {
        try {
            final String message = ((ResponseErrorModel) objectError).getMessenge();
            String code = ((ResponseErrorModel) objectError).getCode();
            if (ShareReference.isLogin) {
                if (code.equals(Constant.RESPONE_CODE)) {
                    DialogFactory.createMessageDialogSessionTimeout(getActivity(), message, code);
                } else {
                    showMessage(message);
                }
            } else {
                showMessage(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateUI(Object object) {

    }

    public void showProgressBar() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity)getActivity()).showProgressBar();
            }
        });
    }

    public void hideProgressBar() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity)getActivity()).hideProgressBar();
            }
        });
    }

    public void hideKeyboard() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity)getActivity()).hideKeyboard();
            }
        });
    }

    protected void showMessage(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogFactory.dialog_Message(getActivity(), message);
            }
        });
    }

    public void CheckNetworkRequest(final BaseActivity baseActivity, final CheckNetworkListener listener) {
        ((BaseActivity)getActivity()).CheckNetworkRequest(baseActivity, listener);
    }

    public interface CheckNetworkListener {
        void Connected();
    }
}
