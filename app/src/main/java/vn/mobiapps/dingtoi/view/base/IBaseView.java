package vn.mobiapps.dingtoi.view.base;

/**
 * Created by Truong Thien on 10/24/2017.
 */

public interface IBaseView<T> {
    void showLoading();
    void hideLoading();
    void nameRequest(String style);
    void onSuccess(T objectSuccess);
    void onError(T objectError);
    <T> void updateUI(T object);
}
