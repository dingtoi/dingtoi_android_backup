package vn.mobiapps.dingtoi.view.fragment.diamondrating;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.model.model_view.LogModel;
import vn.mobiapps.dingtoi.presenter.scandetail.IScanDetailPresenter;
import vn.mobiapps.dingtoi.presenter.scandetail.ScanDetailPresenterImpl;
import vn.mobiapps.dingtoi.utils.ShareReference;
import vn.mobiapps.dingtoi.view.activity.PhysicalGradingActivity;
import vn.mobiapps.dingtoi.view.base.BaseFragment;
import vn.mobiapps.dingtoi.view.fragment.thankyou.ThankYouFragment;
import vn.mobiapps.dingtoi.widgets.ratingbar.BaseRatingBar;

public class DiamondRatingFragment extends BaseFragment implements View.OnClickListener {
    private TextView txtDeviceName, txtTransactionCode, discripsionStar, btn_submit;
    private LinearLayout lyBack;
    private BaseRatingBar rating;
    private IScanDetailPresenter presenter;
    private int pointPhysicalGrading = 0;

    @Override
    protected int resLayout() {
        return R.layout.fragment_diamond_rating;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new ScanDetailPresenterImpl(this, getActivity());
        }
        if (getArguments() != null) {
            pointPhysicalGrading = getArguments().getInt("PhysicalGrading");
        }
    }

    @Override
    protected void setupFragment(View view) {
        txtDeviceName = view.findViewById(R.id.txtDeviceName);
        txtTransactionCode = view.findViewById(R.id.txtTransactionCode);
        discripsionStar = view.findViewById(R.id.discripsionStar);
        btn_submit = view.findViewById(R.id.btn_submit);
        lyBack = view.findViewById(R.id.lyBack);
        rating = view.findViewById(R.id.rating);

        txtDeviceName.setText(presenter.getDeviceName());
        txtTransactionCode.setText(ShareReference.transactionCode);
        rating.setRating(presenter.diamondRating(pointPhysicalGrading));
        int ratingNumber = (int) rating.getRating();
        setContentStar(ratingNumber);
        writeLogFile();
    }

    @Override
    protected void setOnClickListenerFragment() {
        lyBack.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.lyBack:
                ((PhysicalGradingActivity) getActivity()).onBackPressed();
                break;
            case R.id.btn_submit:
                ThankYouFragment fragment = new ThankYouFragment();
                ((PhysicalGradingActivity) getActivity()).pushFragment(fragment);
                break;
            default:
                break;
        }
    }

    private void setContentStar(int star) {
        switch (star) {
            case 1:
                discripsionStar.setText(getString(R.string.star1));
                break;
            case 2:
                discripsionStar.setText(getString(R.string.star2));
                break;
            case 3:
                discripsionStar.setText(getString(R.string.star3));
                break;
            case 4:
                discripsionStar.setText(getString(R.string.star4));
                break;
            case 5:
                discripsionStar.setText(getString(R.string.star5));
                break;
            default:
                break;
        }
    }

    private void writeLogFile() {
        ArrayList<LogModel> dataLog = new ArrayList<>();
        dataLog.add(new LogModel("1", "Transaction Code", ShareReference.transactionCode));
        dataLog.add(new LogModel("2", "Phone’s operating system - latest version", presenter.latestVersion()));
        dataLog.add(new LogModel("3", "Phone’s processor", presenter.hardware()));
        double storageUsed = presenter.getTotalInternalMemorySize() - presenter.getFreeDiskStorage();
        double roundStorageUsed = Math.round(storageUsed * 100.0) / 100.0;
        dataLog.add(new LogModel("4", "Storage capacity of phone: ", roundStorageUsed + " GB of " + presenter.getTotalInternalMemorySize() +" GB Used"));

        dataLog.add(new LogModel("5", "Camera", presenter.hasBackCamera() + ""));
        dataLog.add(new LogModel("6", "Flash", presenter.isFlashSupported() + ""));
        dataLog.add(new LogModel("4", "Volume", presenter.hasControlVolume() + ""));
        dataLog.add(new LogModel("8", "Speaker", presenter.hasAudioOutput() + ""));
        dataLog.add(new LogModel("10", "Wi-Fi", presenter.isWifiSupported() + ""));
        dataLog.add(new LogModel("11", "Bluetooth", presenter.isBluetoothSupported() + ""));
        dataLog.add(new LogModel("12", "Finger Scanner", presenter.isHasFingterprinter() + ""));
        dataLog.add(new LogModel("13", "Outbound calls", presenter.hasTelephony() + ""));
        dataLog.add(new LogModel("14", "Inbound calls", presenter.hasTelephony() + ""));
        dataLog.add(new LogModel("15", "Outbound sms", presenter.hasGsmOrCdma() + ""));
        dataLog.add(new LogModel("16", "Inbound sms", true + ""));
        dataLog.add(new LogModel("17", "Physical Grading", pointPhysicalGrading + ""));
        int ratingNumber = (int) rating.getRating();
        dataLog.add(new LogModel("18", "Diamond Rating Number", ratingNumber + ""));
        presenter.writeLogFile(dataLog);
    }
}
