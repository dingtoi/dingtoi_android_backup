package vn.mobiapps.dingtoi.view.fragment.physicalgrading;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.view.activity.PhysicalGradingActivity;
import vn.mobiapps.dingtoi.view.base.BaseFragment;
import vn.mobiapps.dingtoi.view.fragment.diamondrating.DiamondRatingFragment;

public class PhysicalGradingFragment extends BaseFragment implements View.OnClickListener{
    private CheckBox cbGradeA, cbGradeB, cbGradeC, cbGradeD;
    private LinearLayout lyGradeA, lyGradeB, lyGradeC, lyGradeD;
    private TextView btbNext;
    private LinearLayout lyBack;
    private int pointPhysicalGrading = 50;

    @Override
    protected int resLayout() {
        return R.layout.fragment_physical_grading;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        cbGradeA = view.findViewById(R.id.cbGradeA);
        cbGradeB = view.findViewById(R.id.cbGradeB);
        cbGradeC = view.findViewById(R.id.cbGradeC);
        cbGradeD = view.findViewById(R.id.cbGradeD);
        btbNext = view.findViewById(R.id.btbNext);
        lyBack = view.findViewById(R.id.lyBack);
        lyGradeA = view.findViewById(R.id.lyGradeA);
        lyGradeB = view.findViewById(R.id.lyGradeB);
        lyGradeC = view.findViewById(R.id.lyGradeC);
        lyGradeD = view.findViewById(R.id.lyGradeD);
    }

    @Override
    protected void setOnClickListenerFragment() {
        cbGradeA.setOnClickListener(this);
        cbGradeB.setOnClickListener(this);
        cbGradeC.setOnClickListener(this);
        cbGradeD.setOnClickListener(this);
        btbNext.setOnClickListener(this);
        lyBack.setOnClickListener(this);
        lyGradeA.setOnClickListener(this);
        lyGradeB.setOnClickListener(this);
        lyGradeC.setOnClickListener(this);
        lyGradeD.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.lyBack:
                ((PhysicalGradingActivity)getActivity()).onBackPressed();
                break;
            case R.id.lyGradeA:
            case R.id.cbGradeA:
                setChecked("GradeA");
                break;
            case R.id.lyGradeB:
            case R.id.cbGradeB:
                setChecked("GradeB");
                break;
            case R.id.lyGradeC:
            case R.id.cbGradeC:
                setChecked("GradeC");
                break;
            case R.id.lyGradeD:
            case R.id.cbGradeD:
                setChecked("GradeD");
                break;
            case R.id.btbNext:
                DiamondRatingFragment fragment = new DiamondRatingFragment();
                Bundle args = new Bundle();
                args.putInt("PhysicalGrading", pointPhysicalGrading);
                fragment.setArguments(args);
                ((PhysicalGradingActivity)getActivity()).pushFragment(fragment);
                break;
            default:
                break;
        }
    }

    private void setChecked(String checkBox) {
        switch (checkBox) {
            case "GradeA":
                pointPhysicalGrading = 50;
                cbGradeA.setChecked(true);
                cbGradeB.setChecked(false);
                cbGradeC.setChecked(false);
                cbGradeD.setChecked(false);
                break;
            case "GradeB":
                pointPhysicalGrading = 40;
                cbGradeA.setChecked(false);
                cbGradeB.setChecked(true);
                cbGradeC.setChecked(false);
                cbGradeD.setChecked(false);
                break;
            case "GradeC":
                pointPhysicalGrading = 30;
                cbGradeA.setChecked(false);
                cbGradeB.setChecked(false);
                cbGradeC.setChecked(true);
                cbGradeD.setChecked(false);
                break;
            case "GradeD":
                pointPhysicalGrading = 10;
                cbGradeA.setChecked(false);
                cbGradeB.setChecked(false);
                cbGradeC.setChecked(false);
                cbGradeD.setChecked(true);
                break;
            default:
                break;
        }
    }
}
