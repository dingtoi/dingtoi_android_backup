package vn.mobiapps.dingtoi.view.fragment.scandetail;

import vn.mobiapps.dingtoi.view.base.IBaseView;

public interface IScanDetailView extends IBaseView {
    /*Check Text Inbound*/
    void checkTextInbound();
    /*Check Voice Inbound*/
    void checkVoiceInbound();
}
