package vn.mobiapps.dingtoi.view.fragment.scandetail;

import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.adapter.ScanAdapter;
import vn.mobiapps.dingtoi.model.model_data.PhoneModel;
import vn.mobiapps.dingtoi.model.model_view.ScanerDetailModel;
import vn.mobiapps.dingtoi.model.model_view.ScanerModel;
import vn.mobiapps.dingtoi.presenter.scandetail.IScanDetailPresenter;
import vn.mobiapps.dingtoi.presenter.scandetail.ScanDetailPresenterImpl;
import vn.mobiapps.dingtoi.utils.ShareReference;
import vn.mobiapps.dingtoi.view.activity.PhysicalGradingActivity;
import vn.mobiapps.dingtoi.view.activity.ScannerDetailActivity;
import vn.mobiapps.dingtoi.view.base.BaseActivity;
import vn.mobiapps.dingtoi.view.base.BaseFragment;

public class ScannerDetailFragment extends BaseFragment implements IScanDetailView, View.OnClickListener {
    private IScanDetailPresenter presenter;
    private TextView txtDeviceName, txtTransactionCode, btnNext;
    private LinearLayout lyBack;
    private ArrayList<ScanerModel> listInfo;
    private ScanAdapter adapter;
    private ListView lvDeviceInfo;
    private String nameRequest;
    private Boolean isTextInbound = false, isVoiceInbound = false;

    @Override
    protected int resLayout() {
        return R.layout.fragment_scan_detail;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new ScanDetailPresenterImpl(this, getActivity());
        }
    }

    @Override
    protected void setupFragment(View view) {
        lvDeviceInfo = view.findViewById(R.id.lvDeviceInfo);
        txtDeviceName = view.findViewById(R.id.txtDeviceName);
        txtTransactionCode = view.findViewById(R.id.txtTransactionCode);
        lyBack = view.findViewById(R.id.lyBack);
        btnNext = view.findViewById(R.id.btnNext);
        this.listInfoDevice();
        txtDeviceName.setText(presenter.getDeviceName());
        txtTransactionCode.setText(ShareReference.transactionCode);
    }

    @Override
    protected void setOnClickListenerFragment() {
        lyBack.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.lyBack:
                ((ScannerDetailActivity)getActivity()).onBackPressed();
                break;
            case R.id.btnNext:
                Intent intent = new Intent(getActivity(), PhysicalGradingActivity.class);
                getActivity().startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkTextInbound();
    }


    private void listInfoDevice() {
        showLoading();
        listInfo = new ArrayList<>();
        /*Sprint 1*/
        /*Hardware verification*/
        ArrayList<ScanerDetailModel> listHardware = new ArrayList<>();
        listHardware.add(new ScanerDetailModel("Phone’s operating system - latest version", presenter.latestVersion(), false));
        listHardware.add(new ScanerDetailModel("Phone’s processor", presenter.hardware(), false));
        double storageUsed = presenter.getTotalInternalMemorySize() - presenter.getFreeDiskStorage();
        double roundStorageUsed = Math.round(storageUsed * 100.0) / 100.0;
        listHardware.add(new ScanerDetailModel("Storage capacity of phone", roundStorageUsed + " GB of " + presenter.getTotalInternalMemorySize() +" GB Used", false));
        listInfo.add(new ScanerModel(R.drawable.icon_hardware, "Hardware verification", "", listHardware, true));
        /*Check Camera + Flash*/
        ArrayList<ScanerDetailModel> listCamer_Flash = new ArrayList<>();
        listCamer_Flash.add(new ScanerDetailModel("Camera", "", presenter.hasBackCamera()));
        listCamer_Flash.add(new ScanerDetailModel("Flash", "", presenter.isFlashSupported()));
        listInfo.add(new ScanerModel(R.drawable.icon_camera, "Camera + Flash", "", listCamer_Flash, true));
        /*Check Volume*/
        ArrayList<ScanerDetailModel> listVolume = new ArrayList<>();
        listVolume.add(new ScanerDetailModel("Volume", "", presenter.hasControlVolume()));
        listVolume.add(new ScanerDetailModel("Speaker", "", presenter.hasAudioOutput()));
        listInfo.add(new ScanerModel(R.drawable.icon_volume, "Volume adjustment", "", listVolume, true));
        /*Check Wi-Fi + Bluetooth + Finger Scanner*/
        listInfo.add(new ScanerModel(true,"WIFI WORKING"));
        listInfo.add(new ScanerModel(R.drawable.icon_wifi, "Wi-Fi", "", null, presenter.isWifiSupported()));
        listInfo.add(new ScanerModel(R.drawable.icon_bluetooth, "Bluetooth", "", null, presenter.isBluetoothSupported()));
        listInfo.add(new ScanerModel(R.drawable.icon_fingerprint, "Finger Scanner", "", null, presenter.isHasFingterprinter()));
        /*Check ADVANCE*/
        listInfo.add(new ScanerModel(true,"ADVANCED"));
        /*Check call*/
        ArrayList<ScanerDetailModel> listCall = new ArrayList<>();
        listCall.add(new ScanerDetailModel("Outbound", "", presenter.hasTelephony()));
        listCall.add(new ScanerDetailModel("Inbound", "", this.isVoiceInbound));
        listInfo.add(new ScanerModel(R.drawable.icon_call, "Voice calls", "", listCall, true));
        /*Check SMS*/
        ArrayList<ScanerDetailModel> listSMS = new ArrayList<>();
        listSMS.add(new ScanerDetailModel("Outbound", "", presenter.hasGsmOrCdma()));
        listSMS.add(new ScanerDetailModel("Inbound", "", this.isTextInbound));
        listInfo.add(new ScanerModel(R.drawable.icon_sms, "Texting", "", listSMS, true));

        adapter = new ScanAdapter(getActivity(), R.layout.item_device_info, listInfo);
        lvDeviceInfo.setAdapter(adapter);
        hideLoading();
    }

    @Override
    public void checkTextInbound() {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                String phoneNumber = presenter.getTelephoneNumber();
                presenter.checkTextInbound(new PhoneModel(phoneNumber));
            }
        });
    }

    @Override
    public void checkVoiceInbound() {
        CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
            @Override
            public void Connected() {
                String phoneNumber = presenter.getTelephoneNumber();
                presenter.checkVoiceInbound(new PhoneModel(phoneNumber));
            }
        });
    }

    @Override
    public void nameRequest(String style) {
        this.nameRequest = style;
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        if (this.nameRequest.equals("checkTextInbound")) {
            this.isTextInbound = true;
            checkVoiceInbound();
        } else if (this.nameRequest.equals("checkVoiceInbound")) {
            this.isVoiceInbound = true;
            this.listInfoDevice();
        }
    }

    @Override
    public void onError(Object objectError) {
        if (this.nameRequest.equals("checkTextInbound")) {
            this.isTextInbound = false;
            checkVoiceInbound();
        } else if (this.nameRequest.equals("checkVoiceInbound")) {
            this.isVoiceInbound = false;
            this.listInfoDevice();
        }
        //super.onError(objectError);
    }
}
