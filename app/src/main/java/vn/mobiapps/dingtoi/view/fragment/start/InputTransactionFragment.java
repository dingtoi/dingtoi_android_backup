package vn.mobiapps.dingtoi.view.fragment.start;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.presenter.scandetail.IScanDetailPresenter;
import vn.mobiapps.dingtoi.presenter.scandetail.ScanDetailPresenterImpl;
import vn.mobiapps.dingtoi.view.activity.ScannerDetailActivity;
import vn.mobiapps.dingtoi.view.activity.TouchScreenActivity;
import vn.mobiapps.dingtoi.view.base.BaseActivity;
import vn.mobiapps.dingtoi.view.base.BaseFragment;
import vn.mobiapps.dingtoi.widgets.PinEntryEditText;

import static vn.mobiapps.dingtoi.utils.ShareReference.transactionCode;

public class InputTransactionFragment extends BaseFragment implements IInputTransactionView {
    private Button btn_next;
    private EditText txtPin;
    private TextView txtFail;
    private IScanDetailPresenter presenter;
    @Override
    protected int resLayout() {
        return R.layout.fragment_input_transaction;
    }

    @Override
    protected void createFragment() {
        if (presenter == null) {
            presenter = new ScanDetailPresenterImpl(this, getActivity());
        }
    }

    @Override
    protected void setupFragment(View view) {
        btn_next = view.findViewById(R.id.btn_next);
        txtPin = view.findViewById(R.id.txtPin);
        txtFail = view.findViewById(R.id.txtFail);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkTransactionCode();
            }
        });
    }

    @Override
    public void checkTransactionCode() {
        if (!txtPin.getText().toString().equals("")) {
            CheckNetworkRequest((BaseActivity) getActivity(), new CheckNetworkListener() {
                @Override
                public void Connected() {
                    presenter.checkTransactionCode(txtPin.getText().toString());
                }
            });
        } else {
            txtFail.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccess(Object objectSuccess) {
        txtFail.setVisibility(View.INVISIBLE);
        transactionCode = txtPin.getText().toString();
        Intent intent = new Intent(getActivity(), TouchScreenActivity.class);
        getActivity().startActivity(intent);
    }

    @Override
    public void onError(Object objectError) {
        //super.onError(objectError);
        txtFail.setVisibility(View.VISIBLE);
    }
}
