package vn.mobiapps.dingtoi.view.fragment.start;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.view.activity.StartActivity;
import vn.mobiapps.dingtoi.view.base.BaseFragment;

public class StartFragment extends BaseFragment {
    private String TAG = "StartFragment";
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 7777;

    private Button btn_start;
    @Override
    protected int resLayout() {
        return R.layout.fragment_start;
    }

    @Override
    protected void createFragment() {
        initPermission();
    }

    @Override
    protected void setupFragment(View view) {
        btn_start = view.findViewById(R.id.btn_start);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputTransactionFragment fragment = new InputTransactionFragment();
                ((StartActivity)getActivity()).pushFragment(fragment);
            }
        });
    }

    /*Request Permission*/
    public void initPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && getActivity().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
                /*&& getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED*/) {
                if (this.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Log.v(TAG, "Permission isn't granted");
                } else if (this.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Log.v(TAG, "Permission isn't granted");
                } else if (this.shouldShowRequestPermissionRationale(Manifest.permission.READ_PHONE_STATE)) {
                    Log.v(TAG, "Permission isn't granted");
                }else {
                    Log.v(TAG, "Permisson don't granted and dont show dialog again");
                }
                this.requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE/*, Manifest.permission.CAMERA*/
                }, REQUEST_WRITE_EXTERNAL_STORAGE);
            } else {
                //Exits Permision
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                        Log.e(TAG, "Permision is Granted");
                    }
                } else {
                    initPermission();
                }
                break;
            }
            default: {
            }
        }
    }
}
