package vn.mobiapps.dingtoi.view.fragment.thankyou;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.view.activity.StartActivity;
import vn.mobiapps.dingtoi.view.base.BaseFragment;

public class ThankYouFragment extends BaseFragment implements View.OnClickListener {
    private TextView btn_scan_again;
    @Override
    protected int resLayout() {
        return R.layout.fragment_thank_you;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        btn_scan_again = view.findViewById(R.id.btn_scan_again);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btn_scan_again.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_scan_again:
                Intent intent = new Intent(getActivity(), StartActivity.class);
                intent.putExtra("isScanAgain", true);
                getActivity().startActivity(intent);
                getActivity().finish();
                break;
            default:
                break;
        }
    }
}
