package vn.mobiapps.dingtoi.view.fragment.touchscreen;

import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.presenter.scandetail.paint.PaintView;
import vn.mobiapps.dingtoi.view.activity.ScannerDetailActivity;
import vn.mobiapps.dingtoi.view.base.BaseFragment;

import static android.content.Context.WINDOW_SERVICE;

public class TouchScreenFragment extends BaseFragment implements View.OnClickListener {
    private PaintView paintView;
    private ImageView iconTouch;
    private TextView btnNext, txtTouch;
    private LinearLayout lyFooter, lyTouch;

    @Override
    protected int resLayout() {
        return R.layout.fragment_touch_screen;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        paintView = view.findViewById(R.id.paintView);
        iconTouch = view.findViewById(R.id.iconTouch);
        btnNext = view.findViewById(R.id.btnNext);
        lyFooter = view.findViewById(R.id.lyFooter);
        txtTouch = view.findViewById(R.id.txtTouch);
        lyTouch = view.findViewById(R.id.lyTouch);

        WindowManager wm = (WindowManager) getActivity().getSystemService(WINDOW_SERVICE);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        paintView.init(displayMetrics, new PaintView.Listener.Action() {
            @Override
            public void onActionUp() {
                lyFooter.setVisibility(View.VISIBLE);
            }

            @Override
            public void onActionDown() {
                lyFooter.setVisibility(View.GONE);
                txtTouch.setVisibility(View.GONE);
                iconTouch.setVisibility(View.GONE);
                lyTouch.setVisibility(View.GONE);
            }
        });
        paintView.normal();
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnNext:
                Intent intent = new Intent(getActivity(), ScannerDetailActivity.class);
                getActivity().startActivity(intent);
                break;
            default:
                break;
        }
    }
}
