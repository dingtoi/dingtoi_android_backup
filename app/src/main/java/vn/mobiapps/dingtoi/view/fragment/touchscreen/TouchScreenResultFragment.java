package vn.mobiapps.dingtoi.view.fragment.touchscreen;

import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.view.activity.ScannerDetailActivity;
import vn.mobiapps.dingtoi.view.activity.TouchScreenActivity;
import vn.mobiapps.dingtoi.view.base.BaseFragment;

public class TouchScreenResultFragment extends BaseFragment implements View.OnClickListener {
    private TextView btnNext;
    LinearLayout lyBack;

    @Override
    protected int resLayout() {
        return R.layout.fragment_touch_screen_success;
    }

    @Override
    protected void createFragment() {

    }

    @Override
    protected void setupFragment(View view) {
        btnNext = view.findViewById(R.id.btnNext);
        lyBack = view.findViewById(R.id.lyBack);
    }

    @Override
    protected void setOnClickListenerFragment() {
        btnNext.setOnClickListener(this);
        lyBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.lyBack:
                ((TouchScreenActivity)getActivity()).onBackPressed();
                break;
            case R.id.btnNext:
                Intent intent = new Intent(getActivity(), ScannerDetailActivity.class);
                getActivity().startActivity(intent);
                break;
            default:
                break;
        }
    }
}
