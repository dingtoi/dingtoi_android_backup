package vn.mobiapps.dingtoi.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Button;
import vn.mobiapps.dingtoi.R;

@SuppressLint("AppCompatCustomView")
public class ButtonPlus extends Button {
    private static final String TAG = "Button";

    public ButtonPlus(Context context) {
        super(context);
    }

    public ButtonPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public ButtonPlus(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.ButtonPlus);
        String customFont = a.getString(R.styleable.ButtonPlus_buttonfont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface typeface = null;
        try {
            typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/" + asset);
            setTypeface(typeface);
        } catch (Exception e) {
            Log.e(TAG, "--> Unable to load typeface font: "+e.getMessage());
            return false;
        }
        setTypeface(typeface);
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e){
        if(e.getAction() == MotionEvent.ACTION_DOWN){
            this.getBackground().setColorFilter(0x4DFFFFFF, PorterDuff.Mode.MULTIPLY);
        }else if(e.getAction() == MotionEvent.ACTION_UP){
            this.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
        }
        return super.onTouchEvent(e);
    }
}
