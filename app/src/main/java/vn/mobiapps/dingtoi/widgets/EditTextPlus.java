package vn.mobiapps.dingtoi.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;
import vn.mobiapps.dingtoi.R;

@SuppressLint("AppCompatCustomView")
public class EditTextPlus extends EditText {
    private static final String TAG = "Edittext";
    private boolean changePosition = false;
    private boolean inputMuonth = false;

    public void setNoChangePosition(boolean value){
        this.changePosition = value;
    }

    public EditTextPlus(Context context) {
        super(context);
    }

    public EditTextPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public EditTextPlus(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.EditTextPlus);
        String customFont = a.getString(R.styleable.EditTextPlus_editfont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface typeface = null;
        try {
            typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/" + asset);
            setTypeface(typeface);
        } catch (Exception e) {
            Log.e(TAG, "--> Unable to load typeface font: "+e.getMessage());
            return false;
        }
        setTypeface(typeface);
        return true;
    }

    /*@Override
    public void onSelectionChanged(int start, int end) {
        if (changePosition){
            CharSequence text = getText();
            if (text != null) {
                if (start != text.length() || end != text.length()) {
                    setSelection(text.length(), text.length());
                    return;
                }
            }
        }
        super.onSelectionChanged(start, end);
    }*/

}
