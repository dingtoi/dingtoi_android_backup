package vn.mobiapps.dingtoi.widgets.dialog;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.utils.Constant;
import vn.mobiapps.dingtoi.view.base.BaseActivity;

public class DialogFactory {

    public interface DialogListener {
        interface RetryNetListener {
            void retry(TranslucentDialog dialog);
        }

        interface LogoutListener {
            void logOut();
        }
    }

    public static void dialog_TryNetAgain(final BaseActivity context, String message, final DialogListener.RetryNetListener listener) {
        final TranslucentDialog dialog = new TranslucentDialog(context);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_retry_net);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.txtContent);
        tvMessage.setText(message);
        TextView btTryNet = (TextView) dialog.findViewById(R.id.btTryNet);
        TextView btSetup = (TextView) dialog.findViewById(R.id.btSetup);
        btSetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_MAIN, null);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                intent.setComponent(cn);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        btTryNet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.retry(dialog);
            }
        });
        dialog.show();
    }

    public static void dialog_Message(Context context, String message) {
        final TranslucentDialog dialog = new TranslucentDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_messenger);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.txtContentRegister);
        tvMessage.setText(message);
        TextView btnQuit = (TextView) dialog.findViewById(R.id.btn_OK_DialogRegister);
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void dialog_Logout(Context context, String message, final DialogListener.LogoutListener listener) {
        final TranslucentDialog dialog = new TranslucentDialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        TextView tvMessage = dialog.findViewById(R.id.txtContent);
        tvMessage.setText(message);
        TextView btnYes = dialog.findViewById(R.id.btYes);
        TextView btnNo = dialog.findViewById(R.id.btNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.logOut();
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void createMessageDialogSessionTimeout(final Activity context, final String message, final String code) {
        try {
            final TranslucentDialog dialog = new TranslucentDialog(context);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_messenger);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            final TextView tvMessage = (TextView) dialog.findViewById(R.id.txtContentRegister);
            tvMessage.setText(message);
            TextView btnQuit = (TextView) dialog.findViewById(R.id.btn_OK_DialogRegister);
            btnQuit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToLogin(code, context);
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void goToLogin(String reponseCode, Activity activity) {
        try {
            if (reponseCode.equals(Constant.RESPONE_CODE)) {
//                Intent intent = new Intent(activity, LoginActivity.class);
//                activity.startActivity(intent);
//                activity.finish();
//                activity.overridePendingTransition(R.anim.pop_enter_menu, R.anim.pop_exit_menu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
